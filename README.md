# PostgreSQL

## Запуск БД PostgreSQL
* 1. Переименование файла `.env.example` на `.env`
* 2. Переименование файла `postgres/.env.example` на `postgres/.env`
* 3. Запуск команды `docker-compose build`
* 4. Запуск команды `docker-compose up -d`

#### Описание
* 1. `.env` содержит название проекта, порта postgreSQL
* 2. `postgres/.env` содержит название, логин и пароль БД

